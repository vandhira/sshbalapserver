#!/bin/bash
# By SSHBalap
# 
# ==================================================

NUMBER_OF_CLIENTS=$(grep -c -E "^### " "/etc/v2ray/trojan.json")
	if [[ ${NUMBER_OF_CLIENTS} == '0' ]]; then
		echo ""
		echo "Data Client tidak tersedia!"
		exit 1
	fi

	clear
	echo ""
	echo " Pilih Client terdaftar yang ingin dihapus"
	echo " Tekan CTRL + C untuk kembali"
	echo " ==============================="
	echo "     Tidak ada User Expired"
	grep -E "^### " "/etc/v2ray/trojan.json" | cut -d ' ' -f 2-3 | nl -s ') '
	until [[ ${CLIENT_NUMBER} -ge 1 && ${CLIENT_NUMBER} -le ${NUMBER_OF_CLIENTS} ]]; do
		if [[ ${CLIENT_NUMBER} == '1' ]]; then
			read -rp "Pilih salah satu [1]: " CLIENT_NUMBER
		else
			read -rp "Pilih salah satu [1-${NUMBER_OF_CLIENTS}]: " CLIENT_NUMBER
		fi
	done
user=$(grep -E "^### " "/etc/v2ray/trojan.json" | cut -d ' ' -f 2 | sed -n "${CLIENT_NUMBER}"p)
exp=$(grep -E "^### " "/etc/v2ray/trojan.json" | cut -d ' ' -f 3 | sed -n "${CLIENT_NUMBER}"p)
sed -i "/^### $user $exp/,/^},{/d" /etc/v2ray/trojan.json
systemctl restart v2ray@trojan
clear
echo " Akun Trojan berhasil dihapus"
echo " =========================="
echo " Client Name : $user"
echo " Expired On  : $exp"
echo " =========================="
echo " By SSHBalap"
