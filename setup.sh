#!/bin/bash
# By SSHBalap
# 
# ==================================================

# Otentikasi Root User
if [ "${EUID}" -ne 0 ]; then
echo "You need to run this script as root"
exit 1
fi
if [ "$(systemd-detect-virt)" == "openvz" ]; then
echo "OpenVZ is not supported"
exit 1
fi

# authentication users
red='\e[1;31m'
green='\e[0;32m'
NC='\e[0m'
MYIP=$(wget -qO- icanhazip.com);
echo "Checking VPS"
IZIN=$( curl https://gitlab.com/vandhira/sshbalapserver/-/raw/main/users/auth | grep $MYIP )
if [ $MYIP = $IZIN ]; then
echo -e "${green}Authentication Success...${NC}"
else
echo -e "${red}Permission Denied!${NC}";
echo "Only for Premium Users"
rm -f setup.sh
exit 0
fi
mkdir /var/lib/premium-script;
echo "Enter the hostname of the VPS subdomain, if not available, click Enter"
read -p "Hostname / Domain: " host
echo "IP=$host" >> /var/lib/premium-script/ipvps.conf
wget https://gitlab.com/vandhira/sshbalapserver/-/raw/main/services/ssh-vpn.sh && chmod +x ssh-vpn.sh && screen -S ssh-vpn ./ssh-vpn.sh
wget https://gitlab.com/vandhira/sshbalapserver/-/raw/main/services/sstp.sh && chmod +x sstp.sh && screen -S sstp ./sstp.sh
wget https://gitlab.com/vandhira/sshbalapserver/-/raw/main/services/wg.sh && chmod +x wg.sh && screen -S wg ./wg.sh
wget https://gitlab.com/vandhira/sshbalapserver/-/raw/main/services/ssr.sh && chmod +x ssr.sh && screen -S ssr ./ssr.sh
wget https://gitlab.com/vandhira/sshbalapserver/-/raw/main/services/ss.sh && chmod +x ss.sh && screen -S ss ./ss.sh
wget https://gitlab.com/vandhira/sshbalapserver/-/raw/main/services/ins-vt.sh && chmod +x ins-vt.sh && screen -S v2ray ./ins-vt.sh
wget https://gitlab.com/vandhira/sshbalapserver/-/raw/main/services/ipsec.sh && chmod +x ipsec.sh && screen -S ipsec ./ipsec.sh
rm -f /root/ssh-vpn.sh
rm -f /root/sstp.sh
rm -f /root/wg.sh
rm -f /root/ss.sh
rm -f /root/ssr.sh
rm -f /root/ins-vt.sh
rm -f /root/go.sh
rm -f /root/ipsec.sh
history -c
echo "1.1" > /home/ver
clear
echo " "
echo "Installation is complete!!"
echo " "
echo "=================================-Autoscript Premium-===========================" | tee -a log-install.txt
echo "" | tee -a log-install.txt
echo "--------------------------------------------------------------------------------" | tee -a log-install.txt
echo ""  | tee -a log-install.txt
echo "   >>> Service & Port"  | tee -a log-install.txt
echo "   - OpenSSH                 : 22"  | tee -a log-install.txt
echo "   - OpenVPN                 : TCP 1194, UDP 2200, SSL 992"  | tee -a log-install.txt
echo "   - Stunnel4                : 443"  | tee -a log-install.txt
echo "   - Dropbear                : 143, 109"  | tee -a log-install.txt
echo "   - Squid Proxy             : 3128, 8080 (limit to IP Server)"  | tee -a log-install.txt
echo "   - Badvpn                  : 7100 - 7900"  | tee -a log-install.txt
echo "   - Nginx                   : 81"  | tee -a log-install.txt
echo "   - Wireguard               : 7070"  | tee -a log-install.txt
echo "   - L2TP/IPSEC VPN          : 1701"  | tee -a log-install.txt
echo "   - PPTP VPN                : 1732"  | tee -a log-install.txt
echo "   - SSTP VPN                : 5555"  | tee -a log-install.txt
echo "   - Shadowsocks-R           : 1443-1543"  | tee -a log-install.txt
echo "   - Shadowsocks-OBFS TLS    : 2443-2543"  | tee -a log-install.txt
echo "   - Shadowsocks-OBFS HTTP   : 3443-3453"  | tee -a log-install.txt
echo "   - V2RAY Vmess TLS         : 4443"  | tee -a log-install.txt
echo "   - V2RAY Vmess None TLS    : 80"  | tee -a log-install.txt
echo "   - V2RAY Vless TLS         : 5443"  | tee -a log-install.txt
echo "   - V2RAY Vless None TLS    : 880"  | tee -a log-install.txt
echo "   - Trojan                  : 6443"  | tee -a log-install.txt
echo ""  | tee -a log-install.txt
echo "   >>> Server Information & Other Features"  | tee -a log-install.txt
echo "   - Timezone                : Asia/Jakarta (GMT +7)"  | tee -a log-install.txt
echo "   - Fail2Ban                : [ON]"  | tee -a log-install.txt
echo "   - Dflate                  : [ON]"  | tee -a log-install.txt
echo "   - IPtables                : [ON]"  | tee -a log-install.txt
echo "   - Auto-Reboot             : [ON]"  | tee -a log-install.txt
echo "   - IPv6                    : [OFF]"  | tee -a log-install.txt
echo "   - Autoreboot On 00.00 GMT +7" | tee -a log-install.txt
echo "   - Installation Log --> /root/log-install.txt"  | tee -a log-install.txt
echo ""  | tee -a log-install.txt
echo "---------------------------------Script Mod By SSHBalap-----------------------------------" | tee -a log-install.txt
echo ""
echo " Reboot 10 Sec"
rm -f setup.sh
#sleep 10
#reboot